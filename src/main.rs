use std::time::Duration;

use anyhow::{Result, anyhow};
use dbus::blocking::{Connection, Proxy};
use std::collections::HashMap;

mod cli_config;
mod groups;

use cli_config::CliConfig;

/// Sends a notification.
fn send_notification(
    proxy: &Proxy<'_, &Connection>,
    replace_id: Option<u32>,
    title: Option<&str>,
    body: Option<&str>,
    timeout: Option<i32>
) -> Result<u32>
{
    // Extract configurable values:
    let replace_id = replace_id.unwrap_or(0);
    let timeout = timeout.unwrap_or(-1);
    let title = title.unwrap_or("");
    let body = body.unwrap_or("");

    // Not-configurable values:
    let app_name = "";
    let icon = "";
    let actions = Vec::<String>::new();
    let hints = HashMap::<i32, i32>::new();

    let (id,): (u32,) = proxy.method_call(
        "org.freedesktop.Notifications",
        "Notify",
        (
            app_name,
            replace_id,
            icon,
            title,
            body,
            actions,
            hints,
            timeout,
        )
    )?;

    Ok(id)
}

fn close_notification(proxy: &Proxy<'_, &Connection>, id: u32) -> Result<()> {
    proxy.method_call(
        "org.freedesktop.Notifications",
        "CloseNotification",
        ( id, )
    )?;
    Ok(())
}

fn main() -> Result<()> {
    // Retrieve CLI configuration:
    let cli_config = CliConfig::new()?;
    // Ugly option tranform:
    let title = match &cli_config.title {
        Some(s) => Some(s.as_ref()),
        None => None,
    };
    let body = match &cli_config.body {
        Some(s) => Some(s.as_ref()),
        None => None,
    };

    // Connect to dbus:
    let conn = Connection::new_session()?;
    let proxy = conn.with_proxy(
        "org.freedesktop.Notifications",
        "/org/freedesktop/Notifications",
        Duration::from_millis(5000)
    );

    let groups_file = groups::get_default_groups_file();

    if let Some(group) = &cli_config.close_group {
        let id = groups::get_group_id(&groups_file, group)
            .ok_or_else(|| anyhow!("Group not found: {}", group))?;
        close_notification(&proxy, id)?;
    } else {
        if title.is_none() && body.is_none() {
            return Err(anyhow!("At least a title or a body must be given"))
        }

        // Eventually, retrieve an existing group id:
        let replace_id = match &cli_config.group {
            Some(group) => groups::get_group_id(&groups_file, group),
            None => None,
        };

        // Send the notification:
        let id = send_notification(
            &proxy,
            replace_id,
            title,
            body,
            cli_config.timeout
        )?;

        // Eventually, store the new group id:
        if let Some(group) = &cli_config.group {
            groups::set_group_id(&groups_file, group.to_owned(), id)?;
        };
    }

    Ok(())
}
