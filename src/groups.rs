use std::path::{PathBuf, Path};
use std::env;
use std::collections::HashMap;
use std::fs;

use anyhow::Result;

/// The default groups filename, stored in the system temp directory.
const DEFAULT_GROUPS_FILENAME: &str = "notif-groups.json";

type Groups = HashMap<String, u32>;

/// Return the default path to the groups file, e.g. on Linux: "/tmp/notif-groups.json".
pub fn get_default_groups_file() -> PathBuf {
    let mut file = env::temp_dir();
    file.push(DEFAULT_GROUPS_FILENAME);
    file
}

/// Parses the given groups file.
pub fn get_groups_from_file(file: &Path) -> Result<Groups> {
    let json = fs::read_to_string(file)?;
    let groups: Groups = serde_json::from_str(&json)?;

    Ok(groups)
}

/// Retrieves a single group id.
pub fn get_group_id(file: &Path, group: &str) -> Option<u32> {
    let groups = get_groups_from_file(file).ok()?;
    groups.get(group).copied()
}

/// Sets a single group id. If the given file does not exist, then creates it.
pub fn set_group_id(file: &Path, group: String, id: u32) -> Result<()> {
    let mut groups = get_groups_from_file(file)
        .unwrap_or_else(|_| HashMap::new());
    groups.insert(group, id);
    save_groups_file(file, &groups)?;

    Ok(())
}

/// Save groups to the given file.
fn save_groups_file(file: &Path, groups: &Groups) -> Result<()> {
    let json = serde_json::to_string_pretty(groups)?;
    fs::write(file, &json)?;
    Ok(())
}
